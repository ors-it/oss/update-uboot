#!/bin/bash
set -x

#WIP

#http://jagannadhteki.blog.com/2013/11/07/u-boot-mibs/
#http://www.denx.de/wiki/pub/U-Boot/Documentation/multi_image_booting_scenarios.pdf
#mkimage -A arm -O linux -T multi -a 0x8000 -e 0x8000 -C gzip -n 'Multi image' -d uImage:uramdisk.image.gz:zynq-microzed-linux.dtb uMulti1
#http://morgue.openinkpot.org/wiki/Documentation/ZImageFormat

img=$1

#TODO:
#Support compression
#Support multi-images
#Support FIT
function checkDTB(){
if [[ $(mkimage -l $img | grep -i multi) ]]; then
	echo "Stubbed for multi-image"
	#http://www.isysop.com/unpacking-and-repacking-u-boot-uimage-files/
else
	dd if=$img of=/tmp/img0 skip=64 iflag=skip_bytes

fi
#mkimage -l $img
}

checkDTB /boot/aImage-3.17.0-rc7-l4

checkDTB /boot/uImage-3.17.0-rc7-l4
